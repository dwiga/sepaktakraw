package com.example.sepaktakraw;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class KejuaraanAdapter extends RecyclerView.Adapter<KejuaraanAdapter.KejuaraanViewHolder> {


    private ArrayList<kejuaraan> dataList;

    public KejuaraanAdapter(ArrayList<kejuaraan> dataList) {
        this.dataList = dataList;
    }

    @Override
    public KejuaraanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.kejuaraan_card, parent, false);
        return new KejuaraanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(KejuaraanViewHolder holder, int position) {
        holder.txtTitle.setText(dataList.get(position).getTitle());
        holder.txtContent.setText(dataList.get(position).getContent());
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class KejuaraanViewHolder extends RecyclerView.ViewHolder{
        private TextView txtTitle, txtContent;

        public KejuaraanViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.kejuaraan_title);
            txtContent = (TextView) itemView.findViewById(R.id.kejuaraan_content);
        }
    }
}
