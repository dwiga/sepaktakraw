package com.example.sepaktakraw;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class KejuaraanActivity extends AppCompatActivity {

    public String readJsonFromAssets(){
        String json = null;
        try {
            InputStream is = getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private RecyclerView recyclerView;
    private KejuaraanAdapter adapter;
    private ArrayList<kejuaraan> kejuaraanArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kejuaraan);

        try {
            JSONObject job = new JSONObject(readJsonFromAssets());
            JSONObject data = job.getJSONObject("data");
            JSONArray kjr = data.getJSONObject("kejuaraan").getJSONArray("data");

            kejuaraanArrayList = new ArrayList<>();
            for (int l = 0; l < kjr.length(); l++){
                String title = kjr.getJSONObject(l).getString("title");
                String content = kjr.getJSONObject(l).getString("content");

                kejuaraanArrayList.add(new kejuaraan(title, content));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        adapter = new KejuaraanAdapter(kejuaraanArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(KejuaraanActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

}
