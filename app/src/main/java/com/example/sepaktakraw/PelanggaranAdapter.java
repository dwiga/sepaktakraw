package com.example.sepaktakraw;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class PelanggaranAdapter extends RecyclerView.Adapter<PelanggaranAdapter.PelanggaranViewHolder>{
    private ArrayList<pelanggaran> dataList;
    public PelanggaranAdapter(ArrayList<pelanggaran> dataList) {
        this.dataList = dataList;
    }

    @Override
    public PelanggaranViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.peraturan_card, parent, false);
        return new PelanggaranViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PelanggaranViewHolder holder, int position) {
        holder.txtContent.setText(dataList.get(position).getContent());
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class PelanggaranViewHolder extends RecyclerView.ViewHolder{
        private TextView txtContent;

        public PelanggaranViewHolder(View itemView) {
            super(itemView);
            txtContent = (TextView) itemView.findViewById(R.id.peraturan_content);
        }
    }
}
