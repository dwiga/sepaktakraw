package com.example.sepaktakraw;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class LapanganActivity extends AppCompatActivity {
    public LapanganActivity() throws JSONException {
    }
    public String readJsonFromAssets(){
        String json = null;
        try {
            InputStream is = getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lapangan);
        try {
            JSONObject job = new JSONObject(readJsonFromAssets());
            JSONObject data = job.getJSONObject("data");
            JSONArray lapangan = data.getJSONObject("lapangan").getJSONArray("data");
            String atas = lapangan.getJSONObject(0).getString("text");
            // Toast.makeText(getApplicationContext(), atas, Toast.LENGTH_SHORT).show();
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
