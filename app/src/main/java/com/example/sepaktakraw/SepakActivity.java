package com.example.sepaktakraw;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SepakActivity extends AppCompatActivity {
    ImageView imgSepak;
    TextView txtSepak;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sepak);

        Bundle extra = getIntent().getExtras();
        String prm = extra.getString("prm");
        imgSepak = (ImageView) findViewById(R.id.imgSepak);
        txtSepak = (TextView) findViewById(R.id.txtSepak);

        if ("mula".equals(prm)){
            String uri = "@drawable/mula";
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            imgSepak.setImageDrawable(res);
            String teks = "teknik dasar ini sendiri biasanya dilakukan oleh tekong kearah lapangan lawan, dan digunakan untuk sebagai tanda dimulainya permaiana. Selain itu juga sepak mula ini pun merupakan salah satu gerakan yang sangat penting sekali dalam permainan ini.";
            txtSepak.setText(teks);
            Toast toast = Toast.makeText(SepakActivity.this, "mula", Toast.LENGTH_SHORT);
            toast.show();
        } else if ("sila".equals(prm)) {
            String uri = "@drawable/sila";
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            imgSepak.setImageDrawable(res);
            String teks = "Untuk teknik dasar yang pertama ada yang namanya sepak sila, dimana teknik dasar ini sendiri menyepak bola dengan menggunakan kaki pada bagian dalam yang berfungsi untuk menerima ataupun menendang bola, mengumpan serta menyerang  balik dari serangan lawan.";
            txtSepak.setText(teks);
            Toast toast = Toast.makeText(this, "sila", Toast.LENGTH_SHORT);
            toast.show();
        } else if ("kuda".equals(prm)) {
            String uri = "@drawable/kuda";
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            imgSepak.setImageDrawable(res);
            String teks = "Teknik dasar yang selanjutnya adalah sepak kuda, dimana teknik ini sendiri ini  menggunakan punggung kaki, yang digunakan supaya bisa mengembalikan bola dari serangan lawan, dimana usaha untuk mengambil bola yang rendah.";
            txtSepak.setText(teks);
            Toast toast = Toast.makeText(this, prm, Toast.LENGTH_SHORT);
            toast.show();
        } else if ("cungkil".equals(prm)) {
            String uri = "@drawable/cungkil";
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            imgSepak.setImageDrawable(res);
            String teks = "lalu ada yang namanya sepak cungkil, dimana teknik dasar ini sendiri menggunakan jari kaki, yang digunakan untuk mengambil bola yang cukup jauh, rendah serta bola yang pantulannya liar.";
            txtSepak.setText(teks);
            Toast toast = Toast.makeText(this, prm, Toast.LENGTH_SHORT);
            toast.show();
        } else if ("menapak".equals(prm)){
            String uri = "@drawable/menapak";
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            imgSepak.setImageDrawable(res);
            String teks = "berikutnya ada yang namanya teknik dasar menapak, dimana teknik ini sendiri menyepak bola dengan menggunakan telapak kaki, dan biasanya digunakan saat ingin melakukan smash ke arah lawan, ataupun digunakan untuk membloking smash  dari lawan, saat menyelematkan bola yang dekat dengan jaring.";
            txtSepak.setText(teks);
            Toast toast = Toast.makeText(this, prm, Toast.LENGTH_SHORT);
            toast.show();
        } else if ("heading".equals(prm)){
            String uri = "@drawable/heading";
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            imgSepak.setImageDrawable(res);
            String teks = "Teknik dasar heading atau main kepala ini sendiri biasanya memainkan bola dengan menggunakan kepala, digunakan saat menerima kembali lemparan bola  yang diberikan oleh pihak lawan, bisa juga digunakan untuk menyelamatkan bola dari serangan lawan.";
            txtSepak.setText(teks);
        } else if ("mendada".equals(prm)){
            String uri = "@drawable/mendada";
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            imgSepak.setImageDrawable(res);
            String teks = "teknik dasar yang selanjutnya adalah mendada, dimana teknik ini sendiri memainkan bola dengan menggunakan dada, dan diguanakan saat ingin mengontrol bola ketika bisa dimaiankan selanjutnya.";
            txtSepak.setText(teks);
        }
    }
}
