package com.example.sepaktakraw;

public class soal {
    int no;
    String question, benar, a, b, c, d;

    public soal(int no, String question, String benar, String a, String b, String c, String d){
        this.no = no;
        this.question = question;
        this.benar = benar;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public int getNo(){
        return no;
    }

    public String getQuestion(){
        return question;
    }

    public String getBenar(){
        return benar;
    }

    public String getA(){
        return a;
    }

    public String getB(){
        return b;
    }

    public String getC(){
        return c;
    }

    public String getD(){
        return d;
    }

    public void setNo(int no){
        this.no = no;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setBenar(String benar) {
        this.benar = benar;
    }

    public void setA(String a) {
        this.a = a;
    }

    public void setB(String b) {
        this.b = b;
    }

    public void setC(String c) {
        this.c = c;
    }

    public void setD(String d) {
        this.d = d;
    }
}
