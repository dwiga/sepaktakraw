Olahraga sendiri merupakan salah satu cara alami yang sering kali dilakukan oleh sebagian besar orang supaya tubuhnya menjadi lebih sehat dan bugar kembali.
Selain itu juga ada banyak sekali berbagai macam jenis olah raga yang ada diseluruh dunia ini, terutama adalah olah raga yang menggunakan yang namanya bola.
Bukti sejarah perkembangan permainan olahraga sepak takraw ini sendiri sudah ada sejak sekitar abad ke 15.

Pada saat kesultanan malaka, dimana pada saat itu Raja Muhammad yang merupakan putra dari sultan Mansur shah ini sendiri tak sengaja terken bola yang terbuat dari rotan yang dimainkan oleh  Tun besar , yaitu putra dari Tun perak pada saat sedang bermain sepak raga.
Saat itu bola yang mengenai Raja Muhammad  tersebut mengenai tutup kepalanya dan menjadi jatuh ketanah, dan dengan seketika membuat Raja Muhammad pun menjadi murka serat membunuh Tun besar.
Setelah peristiwa tersebut saudara dari Tun besar pun ingin melakukan yang namanya balas dendam serta ingin membunuh Raja Muhammad, akan tetapi Tun perak pun berhasil  menahan mereka dan berkata bahwa ia tidak akan  mengangkat Raja Muhammad  tersebut sebagai pewaris dari sultan.
Sedangkan di Bangkok sendiri menyebutkan bahwa permainan sepak takraw ini sendiri sudah ada semenjak masa pemerintahan dari Raja Naresuan yaitu pada tahun sekitar 1590 hingga 1605.
Dimana olah raga ini sendiri mengalami berbagai macam versi dan perubahan, namun sepak takraw modern sendiri pertama kali dilakukan di Thailand  pada sekitar tahun 1740-an.
Sedangkan pada tahun 1866 sendiri asosiasi olah raga Malaysia  pun sudah mulai menggunakan aturan permainan untuk pertandingan olah raga sepak takraw ini.
Dimana  pada  beberapa tahun kemudian asosiasi ini sendiri menerapkan gaya volly yang pertama, lalu barulah pada beberapa tahun kemudian sepaktakrawa ini sudah mulai masuk kedalam kurikulum sekolah yang ada di Malaysia.
Pada sekitar tahun 1940-an, barulah sepak takraw versi modern ini sudah mulai tesebar dinegara bagian Asia  Tenggara beserta berbagai macam aturan formalnya yang sudah dibuat dan ditetapkan.
Barulah olahraga ini sudah mulai resmi dikenal dengan sebutan sepak takraw, dimana dalam bahasa melayu sendiri “takraw” ini memiliki arti menendang, sedangkan kata “tahw”  sendiri merupakan arti dair bola anyaman.
Jadi secara harfiah arti dari sepak takraw ini adalah memiliki arti menendang bola.
Di Indonesia  sendiri sepak takraw ini baru mulai resmi dan berkembang sekitar tahun 1970-an, dimana diawali dengan kunjungan muhibah Singapura  serta Malaysia  yang memperkenalkan olah raga ini, dan didirikannya secara resmi PERSASI pada tahun 1971 yang merupakan induk organisasi olah raga sepak takraw ini.