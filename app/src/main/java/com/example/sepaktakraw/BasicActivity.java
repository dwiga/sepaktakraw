package com.example.sepaktakraw;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubePlayer.Provider;

public class BasicActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    YouTubePlayerView ytp;
    Button btnTchKaki, btnTchKepala, btnTchDada;
    LinearLayout basicLayout;
    private static final int RECOVERY_REQUEST = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);

        ytp = (YouTubePlayerView) findViewById(R.id.vidBasic);
        ytp.initialize(config.YOUTUBE_API_KEY, this);

        btnTchKaki = (Button) findViewById(R.id.btnTchKaki);
        basicLayout = (LinearLayout) findViewById(R.id.basicLayout);
        btnTchKepala = (Button) findViewById(R.id.btnTchKepala);
        btnTchDada = (Button) findViewById(R.id.btnTchDada);

        btnTchKepala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent heading = new Intent(getBaseContext(), SepakActivity.class);
                heading.putExtra("prm", "heading");
                startActivity(heading);
            }
        });

        btnTchDada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent heading = new Intent(getBaseContext(), SepakActivity.class);
                heading.putExtra("prm", "mendada");
                startActivity(heading);
            }
        });

        btnTchKaki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(BasicActivity.this);
                builder.setTitle(R.string.txt_kaki);

                String[] teknik = {"Sepak Mula", "Sepak Sila", "Sepak Kuda", "Sepak Cungkil", "Menapak"};
                builder.setItems(teknik, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent sepak = new Intent(getBaseContext(), SepakActivity.class);
                        switch (which){
                            case 0:
                                sepak.putExtra("prm", "mula");
                                break;
                            case 1:
                                sepak.putExtra("prm", "sila");
                                break;
                            case 2:
                                sepak.putExtra("prm", "kuda");
                                break;
                            case 3:
                                sepak.putExtra("prm", "cungkil");
                                break;
                            case 4:
                                sepak.putExtra("prm", "menapak");
                                break;
                        }
                        startActivity(sepak);
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(BasicActivity.this, MainActivity.class);
                startActivity(home);
            }
        });

    }

    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            player.cueVideo("AA1OYsPugV0"); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
        }
    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            Toast.makeText(this, "error", Toast.LENGTH_LONG).show();
            //String error = String.format(getString(R.string.player_error), errorReason.toString());
            //Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(config.YOUTUBE_API_KEY, this);
        }
    }

    protected Provider getYouTubePlayerProvider() {
        return ytp;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbutton, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent home = new Intent(this, MainActivity.class);
                startActivity(home);
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}