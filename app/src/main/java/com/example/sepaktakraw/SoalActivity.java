package com.example.sepaktakraw;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

public class SoalActivity extends AppCompatActivity {
    public String readJsonFromAssets(){
        String json = null;
        try {
            InputStream is = getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private RecyclerView recyclerView;
    private SoalAdapter adapter;
    private ArrayList<soal> soalArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soal);
        Random rn = new Random();

        try {
            JSONObject job = new JSONObject(readJsonFromAssets());
            JSONObject data = job.getJSONObject("data");
            JSONArray kjr = data.getJSONObject("quiz").getJSONArray("data");

            soalArrayList = new ArrayList<>();
            for (int l = 0; l < 5; l++){
                int random = rn.nextInt(10);

                int no = kjr.getJSONObject(random).getInt("no");
                String question = kjr.getJSONObject(random).getString("soal");
                String benar = kjr.getJSONObject(random).getString("benar");
                String a = kjr.getJSONObject(random).getJSONArray("pilihan").getJSONObject(0).getString("jawaban");
                String b = kjr.getJSONObject(random).getJSONArray("pilihan").getJSONObject(1).getString("jawaban");
                String c = kjr.getJSONObject(random).getJSONArray("pilihan").getJSONObject(2).getString("jawaban");
                String d = kjr.getJSONObject(random).getJSONArray("pilihan").getJSONObject(3).getString("jawaban");

                soalArrayList.add(new soal(no, question, benar, a, b, c, d));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        adapter = new SoalAdapter(soalArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SoalActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
