package com.example.sepaktakraw;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class PeraturanAdapter extends RecyclerView.Adapter<PeraturanAdapter.PeraturanViewHolder>{
    private ArrayList<peraturan> dataList;
    public PeraturanAdapter(ArrayList<peraturan> dataList) {
        this.dataList = dataList;
    }

    @Override
    public PeraturanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.peraturan_card, parent, false);
        return new PeraturanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PeraturanViewHolder holder, int position) {
        holder.txtContent.setText(dataList.get(position).getContent());
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class PeraturanViewHolder extends RecyclerView.ViewHolder{
        private TextView txtContent;

        public PeraturanViewHolder(View itemView) {
            super(itemView);
            txtContent = (TextView) itemView.findViewById(R.id.peraturan_content);
        }
    }
}
