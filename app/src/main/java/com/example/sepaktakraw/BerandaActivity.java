package com.example.sepaktakraw;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class BerandaActivity extends AppCompatActivity {

    TextView txtBeranda;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beranda);

        txtBeranda = (TextView) findViewById(R.id.textBeranda);
        try{
            Resources res = getResources();
            InputStream ips = res.openRawResource(R.raw.beranda);

            byte[] b = new byte[ips.available()];
            ips.read(b);
            txtBeranda.setText(new String(b));
        }catch (Exception e){
            txtBeranda.setText("Gak bisa baca file bos!!");
        }
    }
}
