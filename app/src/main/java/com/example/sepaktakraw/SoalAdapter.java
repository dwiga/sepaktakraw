package com.example.sepaktakraw;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SoalAdapter extends RecyclerView.Adapter<SoalAdapter.SoalViewHolder> {
    private ArrayList<soal> dataList;
    public SoalAdapter(ArrayList<soal> dataList){
        this.dataList = dataList;
    }

    @Override
    public SoalViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.soal_card, parent, false);
        return new SoalAdapter.SoalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SoalAdapter.SoalViewHolder holder, int position) {
        holder.txtQuestion.setText(dataList.get(position).getQuestion());
        holder.txtA.setText(dataList.get(position).getA());
        holder.txtB.setText(dataList.get(position).getB());
        holder.txtC.setText(dataList.get(position).getC());
        holder.txtD.setText(dataList.get(position).getD());
        holder.jawaban = dataList.get(position).getBenar();
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class SoalViewHolder extends RecyclerView.ViewHolder{
        private TextView txtQuestion, txtA, txtB, txtC, txtD;
        private String jawaban;
        private CardView cv;

        public SoalViewHolder(View itemView) {
            super(itemView);
            txtQuestion = (TextView) itemView.findViewById(R.id.question);
            txtA = (TextView) itemView.findViewById(R.id.a);
            txtB = (TextView) itemView.findViewById(R.id.b);
            txtC = (TextView) itemView.findViewById(R.id.c);
            txtD = (TextView) itemView.findViewById(R.id.d);

            cv = (CardView) itemView.findViewById(R.id.cv);

            txtA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (jawaban.equals("A")){
                        txtA.setEnabled(false);
                        txtB.setEnabled(false);
                        txtC.setEnabled(false);
                        txtD.setEnabled(false);
                        cv.setCardBackgroundColor(Color.GREEN);
                    }else{
                        txtA.setEnabled(false);
                        txtB.setEnabled(false);
                        txtC.setEnabled(false);
                        txtD.setEnabled(false);
                        cv.setCardBackgroundColor(Color.RED);
                    }
                }
            });

            txtB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (jawaban.equals("B")){
                        txtA.setEnabled(false);
                        txtB.setEnabled(false);
                        txtC.setEnabled(false);
                        txtD.setEnabled(false);
                        cv.setCardBackgroundColor(Color.GREEN);
                    }else{
                        txtA.setEnabled(false);
                        txtB.setEnabled(false);
                        txtC.setEnabled(false);
                        txtD.setEnabled(false);
                        cv.setCardBackgroundColor(Color.RED);
                    }
                }
            });

            txtC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (jawaban.equals("C")){
                        txtA.setEnabled(false);
                        txtB.setEnabled(false);
                        txtC.setEnabled(false);
                        txtD.setEnabled(false);
                        cv.setCardBackgroundColor(Color.GREEN);
                    }else{
                        txtA.setEnabled(false);
                        txtB.setEnabled(false);
                        txtC.setEnabled(false);
                        txtD.setEnabled(false);
                        cv.setCardBackgroundColor(Color.RED);
                    }
                }
            });

            txtD.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (jawaban.equals("D")){
                        txtA.setEnabled(false);
                        txtB.setEnabled(false);
                        txtC.setEnabled(false);
                        txtD.setEnabled(false);
                        cv.setCardBackgroundColor(Color.GREEN);
                    }else{
                        txtA.setEnabled(false);
                        txtB.setEnabled(false);
                        txtC.setEnabled(false);
                        txtD.setEnabled(false);
                        cv.setCardBackgroundColor(Color.RED);
                    }
                }
            });
        }
    }
}
