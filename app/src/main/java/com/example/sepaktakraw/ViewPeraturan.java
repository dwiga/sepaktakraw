package com.example.sepaktakraw;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class ViewPeraturan extends AppCompatActivity {
    TextView header;

    public String readJsonFromAssets(){
        String json = null;
        try {
            InputStream is = getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private RecyclerView recyclerView;
    private PeraturanAdapter adapter;
    private ArrayList<peraturan> peraturanArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.peraturan_view);

        Bundle extra = getIntent().getExtras();
        String prm = extra.getString("menu");

        header = (TextView) findViewById(R.id.textViewHeader);

        try{
            JSONObject job = new JSONObject(readJsonFromAssets());
            JSONObject data = job.getJSONObject("data");
            JSONArray prt = data.getJSONObject("peraturan").getJSONObject(prm).getJSONArray("body");
            peraturanArrayList = new ArrayList<>();
            for (int l = 0; l < prt.length(); l++){
                String content = prt.getJSONObject(l).getString("data");

                peraturanArrayList.add(new peraturan(content));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        adapter = new PeraturanAdapter(peraturanArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ViewPeraturan.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
