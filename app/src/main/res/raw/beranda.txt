Sepaktakraw adalah permainan sepak raga yang telah dimodivikasi untuk dijadikan sebuah permianan yang kompetitif.
Sepak raga sebagai dasar permainan sepaktakraw adalaholahragapermainan tradisionalIndonesiadimainkan oleh 6 – 7 orang secara melingkar.
Pada periode 1945 – 1986 ada kecendrungan pada periode ini sepak raga lebih digairahkan beberapa propinsi di SULSEL dan beberapa daerah di Sumatra tetap terpelihara.
Pada tahun 1970 datang rombongan pemain sepak takraw dari Malaysia dan beberapa bulan kemudian datang dari Singapura memperkenalkan sepak raga jaring.
Pemerintah dalam hal ini Ditjen Olahraga yang dipimpin oleh Mayjen Supardi, mengembangkan sepaktakraw dengan cikal bakal sepak raga.
Pada tanggal 16 Maret 1970 didirikan organisasi Persatuan Sepak Raga Seluruh Indonesia (PERSERASI) dengan Ketua Umum Drs. Moh. Yunus Akbar, dan pada tangal 6-8 Oktober diadakan kongres I semacam munas yang dihadiri 24 PEMDA.
Pada periode tahun 1987 salah satu putusan Kongres I 1986 ialah pemilihan pengurus besar yang baru yaitu Ir. H. Marjoeni.
Denganhasil keputusan antara lain adalah dirubahnya sebutan “Sepak raga” menjadi “Sepaktakraw”.