package com.example.sepaktakraw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnSejarah, btnBasic, btnLapangan, btnPeraturan, btnPelanggaran, btnKejuaraan, btnSoal;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSejarah = (Button) findViewById(R.id.btn_sejarah);
        btnBasic = (Button) findViewById(R.id.btn_dasar);
        btnLapangan = (Button) findViewById(R.id.btn_lapangan);
        btnPeraturan = (Button) findViewById(R.id.peraturan);
        btnPelanggaran = (Button) findViewById(R.id.pelanggaran);
        btnKejuaraan = (Button) findViewById(R.id.kejuaraan);
        btnSoal = (Button) findViewById(R.id.soal);

        btnSejarah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sejarah = new Intent(MainActivity.this, SejarahActivity.class);
                sejarah.putExtra("menu", "sejarah");
                // sejarah.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(sejarah);
                // finish();
            }
        });

        btnBasic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent basic = new Intent(MainActivity.this, BasicActivity.class);
                basic.putExtra("menu", "basic");
                startActivity(basic);
            }
        });

        btnLapangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lap = new Intent(MainActivity.this, LapanganActivity.class);
                lap.putExtra("menu", "lapangan");
                startActivity(lap);
            }
        });

        btnPeraturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent prt = new Intent(getApplicationContext(), PeraturanActivity.class);
                prt.putExtra("menu", "peraturan");
                startActivity(prt);
            }
        });

        btnPelanggaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent plg = new Intent(getApplicationContext(), PelanggaranActivity.class);
                plg.putExtra("menu", "pelanggaran");
                startActivity(plg);
            }
        });

        btnKejuaraan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent kjr = new Intent(getApplicationContext(), KejuaraanActivity.class);
                kjr.putExtra("menu", "kejuaraan");
                startActivity(kjr);
            }
        });

        btnSoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent soal = new Intent(getApplicationContext(), SoalActivity.class);
                startActivity(soal);
            }
        });
    }

    public void openBeranda(View view){
        Intent beranda = new Intent(this, BerandaActivity.class);
        beranda.putExtra("menu", "beranda");
        startActivity(beranda);
    }
}
