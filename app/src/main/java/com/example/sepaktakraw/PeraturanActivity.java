package com.example.sepaktakraw;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class PeraturanActivity extends AppCompatActivity {
    Button btnUmum, btnPemain, btnAngka;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peraturan);

        btnUmum = (Button) findViewById(R.id.btnPrtUmum);
        btnAngka = (Button) findViewById(R.id.btnPrtAngka);
        btnPemain = (Button) findViewById(R.id.btnPrtPemain);

        btnUmum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent umum = new Intent(PeraturanActivity.this, ViewPeraturan.class);
                umum.putExtra("menu", "umum");
                startActivity(umum);
            }
        });

        btnAngka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent angka = new Intent(PeraturanActivity.this, ViewPeraturan.class);
                angka.putExtra("menu", "angka");
                startActivity(angka);
            }
        });

        btnPemain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pemain = new Intent(PeraturanActivity.this, ViewPeraturan.class);
                pemain.putExtra("menu", "pemain");
                startActivity(pemain);
            }
        });
    }
}
