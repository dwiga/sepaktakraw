package com.example.sepaktakraw;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.InputStream;

public class SejarahActivity extends AppCompatActivity {
    TextView txtSejarah;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sejarah);

        txtSejarah = (TextView) findViewById(R.id.textSejarah);
        try{
            Resources res = getResources();
            InputStream ips = res.openRawResource(R.raw.sejarah);

            byte[] b = new byte[ips.available()];
            ips.read(b);
            txtSejarah.setText(new String(b));
        }catch (Exception e){
            txtSejarah.setText("Gak bisa baca file bos!!");
        }
    }
}
