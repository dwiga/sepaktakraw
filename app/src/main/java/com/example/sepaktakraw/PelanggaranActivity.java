package com.example.sepaktakraw;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class PelanggaranActivity extends AppCompatActivity {
    TextView header;
    public String readJsonFromAssets(){
        String json = null;
        try {
            InputStream is = getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private RecyclerView recyclerView;
    private PelanggaranAdapter adapter;
    private ArrayList<pelanggaran> pelanggaranArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelanggaran);

        header = (TextView) findViewById(R.id.textViewHeader);
        try{
            JSONObject job = new JSONObject(readJsonFromAssets());
            JSONObject data = job.getJSONObject("data");
            JSONArray prt = data.getJSONObject("pelanggaran").getJSONArray("data");
            pelanggaranArrayList = new ArrayList<>();
            for (int l = 0; l < prt.length(); l++){
                String content = prt.getJSONObject(l).getString("data");

                pelanggaranArrayList.add(new pelanggaran(content));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        adapter = new PelanggaranAdapter(pelanggaranArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(PelanggaranActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
